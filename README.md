
# Digitalisierung in der Schule - Software, Hardware, Dienste und Dateiformate

Software, die in Schulen eingesetzt wird, muss Freie Software sein. Es gibt zwar viele triftige Gründe dafür, aber viele Politiker sind sich der Bedeutung von Software noch nicht vollends bewusst.

In diesem Manifest sprechen wir primär über die Software und Dienste, die von Lehrkräften, Schülerinnen und Schülern verwendet werden. Die Software und Dienste, die zur Verwaltung des Schulbetriebs nötig sind, betrachten wir in unserem Projekt *Public Money, Public Code*.

Freie Software gibt allen das Recht, Programme für jeden Zweck zu verwenden, zu verstehen, zu verbreiten und zu verbessern. Diese Freiheiten stärken andere Grundrechte wie die Redefreiheit, die Pressefreiheit und das Recht auf Privatsphäre.

Die Schülerinnen und Schüler von heute sind die Bürger der Zivilgesellschaft von morgen. Die Wirtschaft, die Zivilgesellschaft und die politischen Institutionen sind schon heute hochkomplex und weitgehend digitalisiert. Darauf soll die Schule sie vorbereiten. 

## Privacy & Surveillance

Die persönlichen Daten der Schülerinnen und Schüler sind besonders schutzbedürftig. Ihre persönlichen Daten und auch die Metadaten ihrer digitalen Aktivitäten dürfen nicht in Ländern übertragen werden, in denen sie nicht den demokratischen Kontrollmöglichkeiten unserer gewählten Vertreter unterliegen. Es gibt starke finanzielle Anreize, diese Informationen abzuschöpfen und ohne Kontrolle der Schülerinnen und Schüler, die auch Konsumenten sind, weiterzuverarbeiten. In den gängigen proprietären Betriebssystemen sind Telemetrie-Funktionen eingebaut, die nicht oder nur mit großem Aufwand deaktiviert werden können. Dass diese Daten unter anderem dazu verwendet werden können, die Software zu verbessern, ist unbestritten. Sie kann aber auch verwendet werden, um individuelle Profile der Nutzerinnen und Nutzer zu erstellen. Sobald diese Informationen den jeweiligen Rechner verlassen haben, besteht keine Möglichkeit mehr, diese Daten zurückzuholen.  

Wir fordern eine kategorische Opt-In-Policy durch die Erziehungsberechtigten/Eltern für jegliche Datenübertragung der Schülerinnen und Schüler an Hersteller oder Dritte. Nur Freie Software, die - bei Bedarf - auch auf eigener Hardware betrieben werden kann, sichert diese Privatheit zu.

## Vendor-Lockin

Es war in der Vergangenheit wiederholt zu beochaten, dass Marktteilnehmer ihre proprietäre Software, Protokolle und Formate dazu benutzen, andere Marktteilnehmer (ihre Konkurrenz) vom Wettbewerb ausschließen. Dazu nutzen diese Marktteilnehmer diskriminierende Lzenzmodelle, Geheimhaltung und Patentierung oder die andauernde, nicht-standardisierte Veränderung von Quasi-Standards. Dadurch wird Wettbewerb verhindert und die Monopolisten können den Schulen ihre überhöhten Preise diktieren.

Durch inkompatible Updates oder nicht-standardisierte Erweiterung von Dateiformaten (angeblich im Namen der Innovation) erzwingen Hersteller proprietärer Dateiformate unerwünschte Updates von Software-Paketen und - da neue Software häufig auch ressourcenhungriger ist - damit auch neuer Hardware.

Netzwerkeffekte sorgen dafür, dass Lehrkräfte, Schülerinnen und Schüler sich bei sozialen Netzwerken und Plattformen, Video-, Messenger-, Fotodiensten, etc., registrieren, selbst wenn diese die Rechte der Nutzerinnen und Nutzer nicht schützen. 

## Im Namen der besten Lösung

Freie Software orientiert sich daran, welche Designs und Architekturen die Bedürfnisse der User am besten erfüllen, nicht daran, welche Features den meisten Gewinn versprechen.

## Sicherheitsupdates für immer

Wenn die Anbieter von Software oder Diensten kein Interessen mehr an der Pflege ihrer Produkte haben, sind die User verloren. Die Schulen wären gezwungen all ihre Infrastruktur (Software, Dienste und in der Folge in der Regel auch Hardware) kostenintensiv zu erneuern, wenn die Auslieferung von Sicherheitsupdates durch den Hersteller eingestellt wird, da sonst der sichere ("secure") Betrieb der Infrastruktur in Frage gestellt ist. Bei Freier Software kann ein beliebiger SW-Anbieter gewählt werden, um eine weitere Pflege der SW-Pakete zu sichern.

## Verstehen statt Benutzen

Freie Software wird nicht einfach nur benutzt. Schülerinnen und Schüler können sie studieren, verbessern und die Verbesserungen mit anderen teilen. Auf diese Weise erlernen sie genau die Komptenzen, die eine moderne Industrienation wie Deutschland benötigt, um im zukünftigen internationalen Wettbewerb bestehen zu können. Proprietäre Software hingegen wird nur benutzt. Alle Nutzer können nur die Funktionen verwenden, die der Hersteller vorgesehen hat. Es herrscht kein Raum für Kreativität, Reparierbarkeit, Verbesserung oder Lernen.

## Verbesserung für alle

Schülerinnen und Schüler können im Schulunterricht verwendete Software gemeinsam erkunden, verbessern und ihre Verbesserungen mit allen anderen Schulen (in Deutschland und der Welt) teilen. Jegliche Verbesserung muss nur *einmal* implementiert werden, um alle profitieren zu lassen.

## Verändern können statt gehorchen müssen

Die Fähigkeit Freie Software verändern zu können und nicht einem vorgefertigten Programmablauf unterworfen zu sein, lehrt eine Kernkompetenz für Bürger in der Zivilgesellschaft. Es ist möglich Dinge zu verändern und verbessern, sich einzubringen und zu engagieren. Diese Fähigkeit verkümmert beim Konsum proprietärer Software.

## Nachhaltigkeit

Freie Software wird länger in Betrieb gehalten und kann für den Betrieb älterer Hardware optimiert werden. Diese verhindert, dass alte, aber noch funktionsfähige Hardware frühzeitig außer Betrieb werden muss. Dies verbessert die Umweltbilanz. Reparierbarkeit, Nachhaltigkeit und langer Betrieb ist keine Priorität von kommerziellen Anbietern. Zudem verhindern große Hardware-Anbieter über Patent-, Trademark- und Wettbewerbsrecht, dass die Schulträger Geräte kostengünstig reparieren können und so die Umweltbilanz verbessern. Wenn auch geplante Obsoleszenz nur selten nachgewiesen werden können, haben doch die Hardware-Anbieter einen Fehlanreiz dazu, Hardware zu produzieren, die nicht besonders langlebig ist.

## Kosten

Freie Software ist oft kostenfrei beziehbar. Anbieter propritärer Lösungen behaupten oft, dass ihre Lösungen über die Lebensdauer der Produkte kostengünstiger wären, als Freie Software. Dies lässt sich für manche Produkte insbesondere dann belegen, wenn Trainingsmaßnahmen für Mitarbeiterinnen und Mitarbeiter nötig werden. Diese Rechnungen, die häufig auch (biased) und (skewed) lassen sich nicht 1:1 auf Schulen übertragen. Besonders im Hinblick auf die soziale Schieflage und Auslese im deutschen Schulsystem, sind Freie Lösungen zu bevorzugen, da sie allen Schülerinnen und Schülern unabhängig vom Einkommen und der sozialen Zugehörigkeit fairen und gleichen Zugang gewähren.

Freie Software muss zwar häufig nicht bezahlt werden, dennoch ist der Einsatz nicht kostenlos. Support, Schulungen, die *freiwillige* finanzielle Unterstützung guter Projekte müssen berücksichtigt werden. Diese Dienstleistungen können von regionalen mittelständischen Unternehmen geleistet werden. So wird die regionale Wirtschaft gefördert, Arbeitsplätze geschaffen und Wertschöpfung erzielt. 

